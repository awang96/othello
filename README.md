Environnement
=============

Etat
------

* Matrice 8x8 avec l'emplacement des pions des 2 joueurs
  * 0 pour un emplacement vide
  * 1 pour les pions du joueur 1
  * 2 pour les pions du joueur 2
* Etat de départ :

| 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
|---|---|---|---|---|---|---|---|
| 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
| 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
| 0 | 0 | 0 | 1 | 2 | 0 | 0 | 0 |
| 0 | 0 | 0 | 2 | 1 | 0 | 0 | 0 |
| 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
| 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
| 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |

Actions
-------

Pour chaque état, 64 actions potentiellement disponibles, chaque action correspondant à un emplacement du plateau.
Les actions possibles sont celles qui respectent les règles suivantes :
* la case doit être vide, représentée par 0
* la case doit être sur le plateau, indice x et y compris entre 0 inclu et 8 exclu
* la case doit capturer des pièces de l'adversaire, une pièce du joueur (représentée par 1) doit se trouver horizontalement, verticalement ou diagonalement de la case
* la case doit être adjacente à une pièce de l'adversaire, représentée par 2

Récompenses
-----------

* Victoire : 50
* Défaite : -50
* Nul: -10
* Impossible: -100

1ère ébauche
------------

Implémentation des agents suivant une stratégie de positionnement
L'agent choisit son coup suivant la valeur des emplacements définie par la matrice suivante :

| 500  | -150 | 30 | 10 | 10 | 30 | -150 | 500  |
|------|------|----|----|----|----|------|------|
| -150 | -250 | 0  | 0  | 0  | 0  | -250 | -150 |
| 30   | 0    | 1  | 2  | 2  | 1  | 0    | 30   |
| 10   | 0    | 2  | 16 | 16 | 2  | 0    | 10   |
| 10   | 0    | 2  | 16 | 16 | 2  | 0    | 10   |
| 30   | 0    | 1  | 2  | 2  | 1  | 0    | 30   |
| -150 | -250 | 0  | 0  | 0  | 0  | -250 | -150 |
| 500  | -150 | 30 | 10 | 10 | 30 | -150 | 500  |

Pour chaque action, la fonction d'évaluation est la somme des valeurs prises de la matrice ci-dessus.
L'agent choisit le coup ayant la meilleure évaluation.

Lancement
---------

```
pip install -r requirements.txt
python dispatcher.py
```
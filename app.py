from kivy.app import App
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.properties import NumericProperty


class Home(Screen):
    pass


class Play(Screen):

    def on_enter(self, *args):
        self.ids.board.starting_setup()


class Train(Screen):
    pass


class Choose(Screen):
    pass


class OthelloApp(App):

    def build(self):
        sm = ScreenManager()
        sm.add_widget(Home(name='home'))
        sm.add_widget(Play(name='play'))
        sm.add_widget(Train(name='train'))
        sm.add_widget(Choose(name='choose'))
        return sm


if __name__ == '__main__':
    OthelloApp().run()

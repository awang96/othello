from kivy.uix.gridlayout import GridLayout
from kivy.properties import NumericProperty
from kivy.uix.button import Button
import copy
import threading
import time

from game.othelloGE import OthelloBoard
from game.dispatcher import ACTIONS, Policy
from game.othelloIA import Player


class Board(GridLayout):
    _o = OthelloBoard()
    _p1_tiles = NumericProperty(_o.get_tiles_number()[0])
    _p2_tiles = NumericProperty(_o.get_tiles_number()[1])
    _current_epoch = NumericProperty(1)
    _player = NumericProperty(0)

    def __init__(self, **kwargs):
        super(Board, self).__init__(cols=8, **kwargs)
        self.board = self.refresh()
        self.update()
        self.policy1 = Policy(ACTIONS, model='p1.joblib')
        self.policy2 = Policy(ACTIONS, model='p2.joblib')
        self.p1 = Player(1, self._o, self.policy1)
        self.p2 = Player(-1, self._o, self.policy2)

    def refresh(self):
        res = list()
        for row_count, row in enumerate(self._o.board):
            res.append(list())
            for col in row:
                if col == 1:
                    res[row_count].append(Player1Button())
                elif col == -1:
                    res[row_count].append(Player2Button())
                elif col == 2:
                    res[row_count].append(PredictedButton())
                else:
                    res[row_count].append(EmptyButton())
        return res

    def update(self):
        self.board = self.refresh()
        self.clear_widgets()
        for row in self.board:
            for col in row:
                self.add_widget(col)

    def train(self, epochs):
        self._current_epoch = 1
        for _ in range(epochs):
            self._o = OthelloBoard()
            self.p1.reset(self._o)
            self.p2.reset(self._o)
            self.update()
            self._p1_tiles, self._p2_tiles = self._o.get_tiles_number()
            while self._p1_tiles + self._p2_tiles != 64:
                self.p1_play()
                self.p2_play()
            self._current_epoch += 1
        self.p1.policy.save_model('p1.joblib')
        self.p2.policy.save_model('p2.joblib')

    def p1_play(self):
        while self.p1.previous_state == self.p1.state:
            action = self.p1.best_action()
            self.p1.do(action)
            self.p1.update_policy()
            self.p2.previous_state = copy.deepcopy(self.p1.state)
            if self.p1.no_move:
                break
        self._p1_tiles, self._p2_tiles = self._o.get_tiles_number()
        self.update()

    def p2_play(self):
        while self.p2.previous_state == self.p2.state:
            action = self.p2.best_action()
            self.p2.do(action)
            self.p2.update_policy()
            self.p1.previous_state = copy.deepcopy(self.p2.state)
            if self.p2.no_move:
                break
        self._p1_tiles, self._p2_tiles = self._o.get_tiles_number()
        self.update()

    def thread_train(self, epochs):
        thread = threading.Thread(target=self.train, args=(epochs,))
        thread.start()

    def restart(self):
        self._o = OthelloBoard()
        self._p1_tiles, self._p2_tiles = self._o.get_tiles_number()
        self.p1.reset(self._o)
        self.p2.reset(self._o)
        self.starting_setup()

    def starting_setup(self):
        if self._player == 1:
            self.set_predictions()
        else:
            self.p1_play()
            self.set_predictions()

    def set_predictions(self):
        moves = list(filter(lambda elem: elem, self._o.get_tiles(self._player)))
        if not moves:
            return False
        predictions = [elem[-1] for elem in moves]
        for row_count, row in enumerate(self._o.board):
            for col_count, col in enumerate(row):
                if dict(x=row_count, y=col_count) in predictions:
                    self._o.board[row_count][col_count] = 2
        self.update()
        return True

    def play_as_p1(self, button):
        for row_count, row in enumerate(self.board):
            for col_count, col in enumerate(row):
                if col == button:
                    move = dict(x=row_count, y=col_count)
                    break
        self._o.play(move, self._player)
        self.reset_predictions()
        self._p1_tiles, self._p2_tiles = self._o.get_tiles_number()
        if self._p1_tiles + self._p2_tiles != 64:
            self.p2.previous_state = copy.deepcopy(self._o.board)
            self.p2_play()
        while not self.set_predictions() and self._p1_tiles + self._p2_tiles != 64:
            self.p2.previous_state = copy.deepcopy(self._o.board)
            self.p2_play()
        if self._p1_tiles + self._p2_tiles == 64:
            self.p2.policy.save_model('p2.joblib')
            self.update()

    def play_as_p2(self, button):
        for row_count, row in enumerate(self.board):
            for col_count, col in enumerate(row):
                if col == button:
                    move = dict(x=row_count, y=col_count)
                    break
        self._o.play(move, self._player)
        self.reset_predictions()
        self._p1_tiles, self._p2_tiles = self._o.get_tiles_number()
        if self._p1_tiles + self._p2_tiles != 64:
            self.p1.previous_state = copy.deepcopy(self._o.board)
            self.p1_play()
        while not self.set_predictions() and self._p1_tiles + self._p2_tiles != 64:
            self.p1.previous_state = copy.deepcopy(self._o.board)
            self.p1_play()
        if self._p1_tiles + self._p2_tiles == 64:
            self.p1.policy.save_model('p1.joblib')
            self.update()

    def reset_predictions(self):
        for row_count, row in enumerate(self._o.board):
            for col_count, col in enumerate(row):
                if col == 2:
                    self._o.board[row_count][col_count] = 0


class BaseButton(Button):
    pass


class Player1Button(BaseButton):
    pass


class Player2Button(BaseButton):
    pass


class PredictedButton(BaseButton):

    def on_release(self):
        if self.parent._player == 1:
            self.parent.play_as_p1(self)
        else:
            self.parent.play_as_p2(self)


class EmptyButton(BaseButton):
    pass

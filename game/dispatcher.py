from game.othelloGE import OthelloBoard
from game.othelloIA import Player
import colorama
from sklearn.neural_network import MLPRegressor
from joblib import dump, load
import numpy as np
import copy


DEFAULT_LEARNING_RATE = 1
DEFAULT_DISCOUNT_FACTOR = 0.5
ACTIONS = [(x, y) for x in range(8) for y in range(8)]


class Policy:
    def __init__(self,
                 actions,
                 learning_rate=DEFAULT_LEARNING_RATE,
                 discount_factor=DEFAULT_DISCOUNT_FACTOR,
                 model=None):
        self.actions = actions
        self.learning_rate = learning_rate
        self.discount_factor = discount_factor
        self.width = 8
        self.height = 8
        self.mlp = self.init_mlp() if not model else load(model)
        self.mlp.fit([[0] * 64], [[0] * 64])
        self.q_vector = None

    def init_mlp(self):
        return MLPRegressor(hidden_layer_sizes=(16,),
                            activation='tanh',
                            learning_rate_init=self.learning_rate,
                            max_iter=1,
                            warm_start=True)

    def save_model(self, name):
        dump(self.mlp, name)

    def state_to_dataset(self, state):
        return np.array([[x / 2 for y in state for x in y]])

    def best_action(self, state):
        self.q_vector = self.mlp.predict(self.state_to_dataset(state))[0]
        return self.actions[np.argmax(self.q_vector)]

    def update(self,
               previous_state,
               last_action,
               reward):
        maxQ = np.amax(self.q_vector)
        last_action = self.actions.index(last_action)
        self.q_vector[last_action] += reward + self.discount_factor * maxQ
        inputs = self.state_to_dataset(previous_state)
        outputs = np.array([self.q_vector])
        self.mlp = self.mlp.fit(inputs, outputs)


if __name__ == '__main__':
    colorama.init()
    board = OthelloBoard()
    policy1 = Policy(ACTIONS, model='player1.joblib')
    policy2 = Policy(ACTIONS, model='player2.joblib')
    player1 = Player(1, board, policy1)
    player2 = Player(2, board, policy2)
    p1_tiles, p2_tiles = 2, 2
    iteration = 0
    while iteration < 100:
        board = OthelloBoard()
        player1.reset(board)
        player2.reset(board)
        board.print_board()
        p1_tiles, p2_tiles = board.get_tiles_number()
        while p1_tiles + p2_tiles != 64:
            while player1.previous_state == player1.state:
                action = player1.best_action()
                player1.do(action)
                player1.update_policy()
                player2.previous_state = copy.deepcopy(player1.state)
                if player1.no_move:
                    break
            print('Player', player1.id, 'plays on ({}, {})'.format(action[0], action[1]))
            p1_tiles, p2_tiles = board.get_tiles_number()
            board.print_board()
            while player2.previous_state == player2.state:
                action = player2.best_action()
                player2.do(action)
                player2.update_policy()
                player1.previous_state = copy.deepcopy(player2.state)
                if player2.no_move:
                    break
            print('Player', player2.id, 'plays on ({}, {})'.format(action[0], action[1]))
            board.print_board()
            p1_tiles, p2_tiles = board.get_tiles_number()
        iteration += 1
        print(player1.score, player2.score)
    player1.policy.save_model('player1.joblib')
    player2.policy.save_model('player2.joblib')

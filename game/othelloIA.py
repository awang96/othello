from random import randint
import copy

BOARD_VALUES = [
    [500, -150, 30, 10, 10, 30, -150, 500],
    [-150, -250, 0, 0, 0, 0, -250, -150],
    [30, 0, 1, 2, 2, 1, 0, 30],
    [10, 0, 2, 16, 16, 2, 0, 10],
    [10, 0, 2, 16, 16, 2, 0, 10],
    [30, 0, 1, 2, 2, 1, 0, 30],
    [-150, -250, 0, 0, 0, 0, -250, -150],
    [500, -150, 30, 10, 10, 30, -150, 500]
]


class Player:
    def __init__(self,
                 id,
                 environment,
                 policy):
        self.id = id
        self.environment = environment
        self.policy = policy
        self.state = self.environment.board
        self.previous_state = self.state
        self.score = 0
        self.reward = 0
        self.last_action = None
        self.no_move = False

    def reset(self, environment):
        self.score = 0
        self.environment = environment
        self.state = self.environment.board
        self.previous_state = self.state

    def best_action(self):
        return self.policy.best_action(self.state)

    def do(self, action):
        self.previous_state = copy.deepcopy(self.state)
        self.reward, self.no_move = self.environment.apply(action, self.id)
        self.score += self.reward
        self.last_action = action

    def update_policy(self):
        self.policy.update(self.previous_state, self.last_action, self.reward)
